import sys

# harel levi
def welcome_Screen():
    """
    Print the logo and number of tries you get

    :param None
    :return: None
    :rtype: None
    """

    HANGMAN_GAME_LOGO = """
     __    __       ___      .__   __.   _______ .___  ___.      ___      .__   __.   
    |  |  |  |     /   \     |  \ |  |  /  _____||   \/   |     /   \     |  \ |  |
    |  |__|  |    /  ^  \    |   \|  | |  |  __  |  \  /  |    /  ^  \    |   \|  |
    |   __   |   /  /_\  \   |  . `  | |  | |_ | |  |\/|  |   /  /_\  \   |  . `  |
    |  |  |  |  /  _____  \  |  |\   | |  |__| | |  |  |  |  /  _____  \  |  |\   |
    |__|  |__| /__/     \__\ |__| \__|  \______| |__|  |__| /__/     \__\ |__| \__|
                                                                                      """
    print(HANGMAN_GAME_LOGO)
    MAX_TRIES = 6
    print(f"you have {MAX_TRIES} tries to win or lose")


def check_win(secret_word, old_letters_guessed):
    """
    This is a boolean function that returns true if all the letters that make up the secret word
    are included in the list of letters that the user guessed. Otherwise, the function returns false.

    :param secret_word: word the user has to guess
    :param old_letters_guessed: list of the letters the player has guessed
    :return: true if all the letters that make up the secret word are in the list of old_letters_guessed otherwise return false
    """
    # looping all the letters in secret word if one of them not int the guessed word we return False otherwise we return True
    for letter in secret_word:
        if letter not in old_letters_guessed:
            return False
    return True


def choose_word(file_path, index):
    """
    the function return the secret word from the file the user sent
    :param file_path: words for the game divided by space
    :param index: Position of a word in the file
    :return: the secret word for guessing
    """
    # open the file at read mode and read the words from the file
    while True:  # check if user file_path correct
        try:
            with open(file_path, 'r') as file:
                words = file.read().split()
        except:
            print(f"sorry, {file_path} is not a corret file name. Please try again!")
            sys.exit()
        while True:  # check if user index is correct
            try:
                secret_word = words[int(index)]
            except:
                print(f"sorry, {index} is not a corret index. Please try again!")
                sys.exit()
            return secret_word


def check_valid_input(letter_guessed, old_letters_guessed):
    """
    function that checks if the guess is valid and if the player havnt guessed it yes

    :param letter_guessed: character from the user.
    :type letter_guessed: str
    :param old_letters_guessed: list of the letters the player have guessed already.
    :return: return True if it is a legal input, else return False
    :rtype: bool
    """
    # if the character is not valid return false
    if letter_guessed.isalpha() is not True or len(letter_guessed) > 1:
        return False
    elif letter_guessed in old_letters_guessed:  # if the character is not in the letters the player has guessed we return false
        return False
    else:
        return True


def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """
    updates the list with the new charecter after cheking if the charecter
    is currect and dont exists in the last guessed by the user return false
    if it didnt work or the character was invalid

    :param letter_guessed: character from the user
    :param old_letters_guessed: list of the letters the player has guessed.
    :return: return True if it is a legal input, else return False
    :rtype: bool
    """
    # check if the input is valid
    if not check_valid_input(letter_guessed, old_letters_guessed):
        print('X')
        old_letters_guessed = ' -> '.join(sorted(old_letters_guessed))
        print(old_letters_guessed)
        return False
    else:
        old_letters_guessed.append(letter_guessed)
        return True


def print_hangman(num_of_tries):
    """Prints the stage of the hangmen with an index.

    :param num_of_tries: a key for the hangman stage
    :type num_of_tries: int
    :return: None
    """

    HANGMAN_PHOTOS = {
        1: """    x-------x""",
        2: """    x-------x
    |
    |
    |
    |
    |""",
        3: """    x-------x
    |       |
    |       0
    |
    |
    |""",
        4: """    x-------x
    |       |
    |       0
    |       |
    |
    |""",
        5: """    x-------x
    |       |
    |       0
    |      /|\\
    |
    |""",
        6: """    x-------x
    |       |
    |       0
    |      /|\\
    |      /
    |""",
        7: """    x-------x
    |       |
    |       0
    |      /|\\
    |      / \\
    |"""
    }
    hangman_status = HANGMAN_PHOTOS[num_of_tries]
    print(hangman_status)  # returning the current situation of the hangman with the given index


def show_hidden_word(secret_word, old_letters_guessed):
    """
    A function that returns a string  of letters and underscores for the guess

    :param secret_word: word the user has to guess
    :param old_letters_guessed: list of the letters the player has guessed.
    :return: reveal the secret word to the player
    """
    # we making the result string by loop all the letters in the secret word
    # if the user guessed it then the word will show and if he didnt '_' will show
    result = ''
    for letter in secret_word:
        if letter in old_letters_guessed:
            result += letter + ' '
        else:
            result += '_ '
    return result


def main():
    # print the welcome screen
    welcome_Screen()
    wrong_guess = 0
    num_of_tries = 1
    old_letters_guessed = list()  # create an empty list for the beginning
    # take from user the file path and index
    file_path = input("Enter file path: ")
    index = input("Enter index: ")
    secret_word = choose_word(file_path, index)
    print("Lets start!")
    print_hangman(num_of_tries)
    print("_ " * len(secret_word))
    # game loop
    while wrong_guess in range(6):
        # take user input
        guessed_letter = input("Guess a letter: ").lower()
        # the main logic of the game
        if try_update_letter_guessed(guessed_letter, old_letters_guessed):
            if guessed_letter not in secret_word:
                wrong_guess += 1
                num_of_tries += 1
                print(":(")
                print_hangman(num_of_tries)
                print(show_hidden_word(secret_word, old_letters_guessed))
            else:
                print(show_hidden_word(secret_word, old_letters_guessed))
                if check_win(secret_word, old_letters_guessed):
                    print("WIN")
                    break
    if wrong_guess == 6:
        if check_win(secret_word, old_letters_guessed):
            print("WIN")
        else:
            print("LOSE")


if __name__ == "__main__":
    main()
